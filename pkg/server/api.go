package server

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/colourdelete/recipes/pkg/ingredients"
)

func APIIngrAddr(c *gin.Context) {
	addr := c.Param("addr")
	if addr == "" {
		var dbg *string = nil
		if gin.Mode() == "debug" {
			dbg = &addr
		}
		c.JSON(400, gin.H{
			"ingrs":  nil,
			"status": "addr not provided or not recognised",
			"debug":  dbg,
		})
		return
	}
	if addr[0] == '/' {
		addr = addr[1:]
	}
	ingrs, err := ingredients.Schollz.FromHTMLURL(addr)
	if err != nil {
		var dbg *string = nil
		if gin.Mode() == "debug" {
			errStr := err.Error()
			dbg = &errStr
		}
		c.JSON(500, gin.H{
			"ingrs":  nil,
			"status": "ingredient list fetch failed",
			"debug":  dbg,
		})
		return
	}
	c.JSON(200, gin.H{
		"ingrs":  ingrs,
		"status": "ingredient list fetch successful",
		"debug":  nil,
	})
}
