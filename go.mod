module gitlab.com/colourdelete/recipes

go 1.15

require (
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-gonic/gin v1.6.3
	github.com/schollz/ingredients v1.1.0
)
